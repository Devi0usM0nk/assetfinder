package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"encoding/json"
)

// Make a request to BinaryEdge Api
func getBinaryEdge(url string) ([]byte, error){
	apikey := cfg.Credentials.BinaryEdge.APIKey

	//Created Http GET Request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil{
		return nil, err
	}

	//Setting necessary Headers
	req.Header.Set("X-Key",apikey)
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil{
		return nil,err
	}

	defer resp.Body.Close()
	return ioutil.ReadAll(resp.Body)
}

//Function to fetch all subdomains 
func fetchBinaryEdge(domain string) ([]string, error){
	url := fmt.Sprintf("https://api.binaryedge.io/v2/query/domains/subdomain/%s",domain)
	body, err := getBinaryEdge(url)
	if err != nil{
		return nil, err
	}

	wrapper := struct{
		    Query string `json:"query"`
		    Page int `json:"page"`
		    Pagesize int `json:"pagesize"`
		    Total int `json:"total"`
		    Events []string `json:"events"`
	}{}
	err = json.Unmarshal(body, &wrapper)
	if err != nil{
		return nil, err
	}

	domains := wrapper.Events

	//Calculating Total Number of Pages
	if wrapper.Total != 0{
		totalPages := wrapper.Total / wrapper.Pagesize
		if wrapper.Total % wrapper.Pagesize != 0{
			totalPages++
		}

		//Getting all pages and appendin it to domains
		for i:=2;i<=totalPages;i++{
			tempURL := fmt.Sprintf("%s?page=%d",url,i)

			body,err = getBinaryEdge(tempURL)
			if err != nil{
				return nil, err
			}
			err = json.Unmarshal(body, &wrapper)
			if err != nil{
				return nil, err
			}
			domains = append(domains, wrapper.Events...)

		}
	}
	return domains, nil
}
